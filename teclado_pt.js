var layoutTeclado = {
	id: "TecladoPrincipal",
	classe: ["TecladoVirtual"],
	classeBaseBotoes: ["TecladoVirtualTecla"],
	linhas: [	{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "q"}, {id: "w"}, {id: "e"}, {id: "r"}, {id: "t"}, {id: "y"}, {id: "u"}, {id: "i"}, {id: "o"}, {id: "p"}, {id: "backspace", label: "←", classe: ["EstiloBackSpace", "TeclaExpandir"]}],
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "a", classe:["EstiloA"]}, {id: "s"}, {id: "d"}, {id: "f"}, {id: "g"}, {id: "h"}, {id: "j"}, {id: "k"}, {id: "l"}, {id: "ç"} ],
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "shift", label: "Shift", classe:["TeclaShift"]}, {id: "z"}, {id: "x"}, {id: "c"}, {id: "v"}, {id: "b"}, {id: "n"}, {id: "m"}, {id: ","}, {id: "."}, {id: "'"}, {id: "shift", label: "Shift", classe:["TeclaShift"]} ],
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "trocarModo", label: "&123", classe:["TeclaTrocarModo"]}, {id: "espaco", label: "", classe:["TeclaEspaco"]}, {id: "trocarModo", label: "&123", classe:["TeclaTrocarModo"]},],
				}
			],
	
}; 

var layoutTecladoMaiusculo = {
	id: "TecladoMaiusculo",
	classe: ["TecladoVirtual"],
	classeBaseBotoes: ["TecladoVirtualTecla"],
	linhas: [	{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "q", label:"Q"}, {id: "w", label:"W"}, {id: "e", label: "E"}, {id: "r", label: "R"}, {id: "t", label:"T"}, {id: "y", label:"Y"}, {id: "u", label:"U"}, {id: "i", label:"I"}, {id: "o", label:"O"}, {id: "p", label:"P"}, {id: "backspace", label: "←", classe: ["EstiloBackSpace", "TeclaExpandir"]}],
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "a", label: "A", classe:["EstiloA"]}, {id: "s", label:"S"}, {id: "d", label:"D"}, {id: "f", label:"F"}, {id: "g", label:"G"}, {id: "h", label:"H"}, {id: "j", label:"J"}, {id: "k", label:"K"}, {id: "l", label:"L"}, {id: "ç", label:"Ç"},],
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "shift", label: "Shift", classe:["TeclaShift"]}, {id: "z", label:"Z"}, {id: "x", label:"X"}, {id: "c", label:"C"}, {id: "v", label:"V"}, {id: "b", label:"B"}, {id: "n", label:"N"}, {id: "m", label:"M"}, {id: ";"}, {id: ":"}, {id: "\""}, {id: "shift", label: "Shift", classe:["TeclaShift"]} ],
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "trocarModo", label: "&123", classe:["TeclaTrocarModo"]}, {id: "espaco", label: "", classe:["TeclaEspaco"]}, {id: "trocarModo", label: "&123", classe:["TeclaTrocarModo"]},],
				}
			],
	
}; 

var layoutTecladoNumerico = {
	id: "TecladoNumerico",
	classe: ["TecladoVirtual"],
	classeBaseBotoes: ["TecladoVirtualTecla", "TecladoNumericoTecla"],
	linhas: [	{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "7"}, {id: "8"}, {id: "9"}]
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "4"}, {id: "5"}, {id: "6"}]
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "1"}, {id: "2"}, {id: "3"}]
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "0", classe: ["TamanhoTeclaZero"]}, {id: ","}]
				},
	
			],
};

var layoutTecladoSimbolo = {
	id: "TecladoSimbolos",
	classe: ["TecladoVirtual"],
	classeBaseBotoes: ["TecladoVirtualTecla", "TecladoNumericoTecla"],
	linhas: [	{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "!"}, {id: "@"}, {id: "%"}, {id: "°"}, {id: "$"}, {id: "*"}]
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "("}, {id: ")"}, {id: "-"}, {id: "_"}, {id: "="}, {id: "+"}]
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "\\"}, {id: ";"}, {id: ":"}, {id: "'"}, {id: "?"}, {id: "/"}]
				},
				{
					classe: ["TecladoVirtualLinha"],
					Teclas: [{id: "trocarModo", label: "&123"}, {id: "espaco", label: "", classe:["TeclaExpandir"]}, {id: "backspace", label: "Backspace"}]
				},
	
			],
};


var tecladoInput = {}; //guarda a relação de qual input certo teclado altera.

var divTecladoMinusculo = document.createElement("div");
var divTecladoMaiusculo = document.createElement("div");
var divTecladoSimbolos = document.createElement("div");

function callbackTeclado(Teclado, Tecla, divTeclado)
{				
	let input_widget = document.getElementById(divTeclado.dataset.inputId);
	if(Teclado == layoutTeclado.id)
	{
		switch(Tecla)
		{
		case "shift":
			divTecladoMinusculo.style.display = "none";
			divTecladoMaiusculo.style.display = "flex";
			break;
		case "backspace":
			let valorAtual = input_widget.value;
			input_widget.value = valorAtual.substring(0, valorAtual.length-1);
			break;				
		case "espaco":
			input_widget.value += " ";
			break;
		case "trocarModo":
			divTecladoMinusculo.style.display = "none";
			divTecladoMaiusculo.style.display = "none";
			divTecladoSimbolos.style.display = "flex";
			break;
		default:
			input_widget.value += Tecla;
			break;
		}
	
	}
	
	if(Teclado == layoutTecladoMaiusculo.id)
	{
		switch(Tecla)
		{
		case "shift":
			divTecladoMinusculo.style.display = "flex";
			divTecladoMaiusculo.style.display = "none";
			break;
		case "backspace":
			let valorAtual = input_widget.value;
			input_widget.value = valorAtual.substring(0, valorAtual.length-1);
			break;
		case "espaco":
			input_widget.value += " ";
			break;
		case "trocarModo":
			divTecladoMinusculo.style.display = "none";
			divTecladoMaiusculo.style.display = "none";
			divTecladoSimbolos.style.display = "flex";
			break;
		default:
			input_widget.value += Tecla.toUpperCase();
			break;
		}
	}
	if(Teclado == layoutTecladoSimbolo.id)
	{
		switch(Tecla)
		{
		default:
			input_widget.value += Tecla.toUpperCase();
			break;
		case "espaco":
			input_widget.value += " ";
			break;
		case "trocarModo":
			divTecladoMinusculo.style.display = "flex";
			divTecladoSimbolos.style.display = "none";
			break;					
		case "backspace":
			let valorAtual = input_widget.value;
			input_widget.value = valorAtual.substring(0, valorAtual.length-1);
			break;
		}
	}
	
	if(Teclado == layoutTecladoNumerico.id)
	{
		input_widget.value += Tecla.toUpperCase();
	}
	input_widget.focus();
	input_widget.setSelectionRange(input_widget.value.length,input_widget.value.length);


	
}

function criarTecladoPtbr(divDestino, inputId)
{
	criarTeclado(layoutTeclado, divTecladoMinusculo);
	criarTeclado(layoutTecladoMaiusculo, divTecladoMaiusculo);
	
	divTecladoMinusculo.dataset.inputId = inputId;
	divTecladoMaiusculo.dataset.inputId = inputId;

	var divTecladoCaracteresEspeciais  = document.createElement("div");
	var divTecladoNumerico = document.createElement("div");

	
	divTecladoSimbolos.appendChild(divTecladoCaracteresEspeciais);
	divTecladoSimbolos.appendChild(divTecladoNumerico);
	

	criarTeclado(layoutTecladoSimbolo, divTecladoCaracteresEspeciais);
	criarTeclado(layoutTecladoNumerico, divTecladoNumerico);
	
	divTecladoCaracteresEspeciais.dataset.inputId = inputId;
	divTecladoNumerico.dataset.inputId = inputId;

	divTecladoMinusculo.style.display = "flex";
	divTecladoMaiusculo.style.display = "none";
	divTecladoSimbolos.style.display = "none";
	
	divDestino.appendChild(divTecladoMinusculo);
	divDestino.appendChild(divTecladoMaiusculo);
	divDestino.appendChild(divTecladoSimbolos);
	
}
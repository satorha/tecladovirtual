//onclick de cada botão
function callbackPressionar()
{
	if(typeof callbackTeclado === "undefined")
		return;

	callbackTeclado(this.parentElement.parentElement.dataset.tecladoId, this.dataset.teclaId, this.parentElement.parentElement);
}

//cria o teclado com o layout especificado dentro da div especificada 
function criarTeclado(layout, divDestino)
{	
	let tecladoDiv = divDestino;
		
	while(divDestino.firstChild)
		divDestino.removeChild(divDestino.firstChild);
		
	for(let i=0; i < layout.classe.length; i++)		
		tecladoDiv.classList.add(layout.classe[i]);
	
	tecladoDiv.dataset.tecladoId = layout.id;
	
	for(let i=0; i < layout.linhas.length; i++)
	{
		let linhaAtualDiv = document.createElement("div");
		
		for(let c=0; c < layout.linhas[i].classe.length; c++)
			linhaAtualDiv.classList.add(layout.linhas[i].classe[c]);
		
		for(let j=0; j < layout.linhas[i].Teclas.length; j++)
		{
			let teclaAtual = layout.linhas[i].Teclas[j];
			let teclaAtualDiv = document.createElement("div");
			
			teclaAtualDiv.onclick = callbackPressionar;
			
			teclaAtualDiv.dataset.teclaId = teclaAtual.id;
			
			for(let c=0; c < layout.classeBaseBotoes.length; c++)
				teclaAtualDiv.classList.add(layout.classeBaseBotoes[c]);			
			
			if(typeof teclaAtual.classe !== "undefined") //campo classe opcional
				for(k=0; k < teclaAtual.classe.length; k++)
					teclaAtualDiv.classList.add(teclaAtual.classe[k]);
			
			if(typeof teclaAtual.label === "undefined") //se label não for informada, mostra o id da tecla
				layout.linhas[i].Teclas[j].label = layout.linhas[i].Teclas[j].id;
			
			teclaAtualDiv.innerText = layout.linhas[i].Teclas[j].label;
			
			linhaAtualDiv.appendChild(teclaAtualDiv);
		}
		tecladoDiv.appendChild(linhaAtualDiv);
	}
	return tecladoDiv;
}


